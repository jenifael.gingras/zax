"""
Swiss knife command to automate boring work tasks.
"""
import os
import logging
from argparse import Namespace
from datetime import datetime

from configargparse import ArgParser
from jira import JIRA
from slack_sdk import WebClient


START_DATETIME = datetime.now()
DEFAULT_CHANNEL = "jeni-test"
JIRA_URL = os.getenv("JIRA_URL")


def create_parser() -> ArgParser:
    parser = ArgParser(prog="zax", description="Swiss knife command to automate boring work tasks.")
    parser.add_argument("--version", action="version", version="%(prog)s 0.1.0")
    return parser


def parse_args() -> Namespace:
    parser = create_parser()
    args = parser.parse_args()
    return args


def create_logger(args: Namespace) -> logging.Logger:
    """ Init a default logger and set level from args. """
    logger = logging.getLogger("zax")
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    return logger


def create_slack_client() -> WebClient:
    slack_token = os.getenv("SLACK_BOT_TOKEN")
    if slack_token is None:
        raise ValueError("SLACK_BOT_TOKEN environment variable is not set.")
    return WebClient(token=slack_token)


def create_jira_client() -> JIRA:
    jira_username = os.getenv("JIRA_USERNAME")
    if jira_username is None:
        raise ValueError("JIRA_USERNAME environment variable is not set.")

    jira_password = os.getenv("JIRA_PASSWORD")
    if jira_password is None:
        raise ValueError("JIRA_PASSWORD environment variable is not set.")

    if JIRA_URL is None:
        raise ValueError("JIRA_URL environment variable is not set.")

    jira_client = JIRA(server=JIRA_URL, basic_auth=(jira_username, jira_password))
    return jira_client


def get_dsp_issues(jira_client: JIRA) -> list:
    """ Get all open DSP issues. """
    issues = jira_client.search_issues("project = DSP AND status = Open")
    return issues


def publish_issues(slack_client: WebClient, issues: list):
    for issue in issues:
        key = issue.key
        title = issue.fields.summary
        url = f"{JIRA_URL}/browse/{key}"
        slack_client.chat_postMessage(channel=DEFAULT_CHANNEL, text=f"<{url}|{key}> {title}", type="mrkdwn")


def main():
    args = parse_args()

    logger = create_logger(args)
    logger.debug("Zax init.")

    slack_client = create_slack_client()
    logger.debug("Slack client created.")

    jira_client = create_jira_client()
    logger.debug("Jira client created.")

    dsp_issues = get_dsp_issues(jira_client)
    publish_issues(slack_client, dsp_issues)


if __name__ == '__main__':
    main()
